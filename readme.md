# shintech/public/nextjs

## Table of Contents
1. [ Synopsis ](#synopsis)
2. [ Installation/Configuration ](#install) <br />
	a. [.env ](#env) <br />
	b. [Development ](#development) <br />
	c. [Production ](#production)
	c. [git hooks ](#git-hooks)

<a name="synopsis"></a>
### Synopsis

Next.JS + Express.JS
  
### Installation

    ./install.sh

<a name="install"></a>
### Installation/Configuration
<a name="env"></a>
#### Copy the config for the environment from config/env/[environment].env and edit as necessary.

    PORT=8000
    NODE_ENV=development

<a name="development"></a>
#### Development

    npm run dev

    # or

    yarn dev

<a name="production"></a>
#### Production
    docker-compose build && docker-compose up -d
    
<a name="git-hooks"></a>
#### git hooks
    ln -s /path/to/repo/config/hooks/hook /path/to/repo/.git/hooks/
