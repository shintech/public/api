FROM node:12.13.0

EXPOSE 8000

RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app

WORKDIR /home/node/app

RUN rm -rv .next log --force && mkdir -p logs 

RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 && \
  chmod +x /usr/local/bin/dumb-init

COPY package.json .
COPY yarn.lock .

USER node

RUN yarn install --production=true
  
COPY --chown=node:node . .

RUN yarn build

ENTRYPOINT ["dumb-init", "--"]

CMD yarn start
