const http = require('http')
const mongoose = require('mongoose')
const { Logger } = require('shintech-utils')
const { ApolloServer, gql } = require('apollo-server-express')
const Server = require('./server')
const Router = require('./server/router')
const typeDefs = require('./server/graphql/typedefs')
const resolvers = require('./server/graphql/resolvers')
const dataSources = require('./server/graphql/datasources')

const NAME = process.env['npm_package_name'] || 'app-name'
const VERSION = process.env['npm_package_version'] || '0.0.1'
const PORT = parseInt(process.env['PORT']) || 8000
const NODE_ENV = process.env['NODE_ENV'] || 'development'
const HOST = process.env['HOST'] || '0.0.0.0'
const REST_API = process.env['REST_API'] || `https://localhost:${PORT}/api`

const dev = NODE_ENV !== 'production'

const logger = new Logger()

logger.info(`starting -> ${NAME} - version: ${VERSION}...`)

const main = async () => {
  const dbOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    connectTimeoutMS: 10000,
    user: process.env['MONGO_USER'],
    pass: process.env['MONGO_PASSWORD']
  }

  try {
    await mongoose.connect(`${process.env['MONGO_URI']}?authSource=admin`, dbOptions)
  } catch (err) {
    logger.error(err.message)
    throw err
  }

  logger.info(`connected to ${process.env['MONGO_URI']}...`)

  const router = new Router()
  const app = Server({ router })
  const server = http.Server(app)

  const apollo = new ApolloServer({
    typeDefs: gql`
      ${typeDefs}
    `,
    resolvers,
    dataSources: dataSources(REST_API),
    debug: NODE_ENV === 'production'
  })

  apollo.applyMiddleware({ app })

  app.set('logger', logger)

  app.use('/api', router)

  server.listen(PORT)
    .on('listening', () => logger.info(`listening at ${HOST}:${PORT}...`))
    .on('error', err => logger.error(err.message))
    .on('close', () => {
      mongoose.connection.close()
    })
}

main()
