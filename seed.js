const mongoose = require('mongoose')
const faker = require('faker')
const User = require('./server/models/User')
const Device = require('./server/models/Device')
const pkg = require('./package.json')

mongoose.Promise = require('bluebird')

const environment = process.env['NODE_ENV'] || 'development'
const connectionString = `${process.env['MONGO_URI']}/${pkg.name}_${environment}`

const createDevices = () => {
  const appleDevices = new Array(5).fill(null).map(() => ({
    model: 'iPad',
    manufacturer: 'Apple',
    description: 'Student iPad'
  }))

  const androidDevices = new Array(5).fill(null).map(() => ({
    model: 'Galaxy Tab',
    manufacturer: 'Samsung',
    description: 'Staff Device'
  }))

  const devices = [ ...androidDevices, ...appleDevices ]

  Device.collection.insertMany(devices, function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Devices data -> Success...')
      mongoose.connection.close()
    }
  })
}

const createUsers = () => {
  const user = new User()

  const users = new Array(5).fill(null).map(() => ({
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName(),
    username: process.env['ADMIN_USER'] || 'username',
    password: user.generateHash(process.env['PASSWORD'] || 'password')
  }))

  User.collection.insertMany(users, function (err, docs) {
    if (err) {
      throw new Error(err)
    } else {
      console.log('Created Users data -> Success...')
      mongoose.connection.close()
    }
  })
}

const dbOptions = {
  useNewUrlParser: true,
  connectTimeoutMS: 10000,
  user: process.env['MONGO_USER'],
  pass: process.env['MONGO_PASSWORD']
}

mongoose.connect(`${connectionString}?authSource=admin`, dbOptions, function (err, res) {
  if (err) { throw new Error(err) }

  console.log('connected to database: ' + connectionString)

  mongoose.connection.db.dropDatabase(() => {
    createUsers()
    createDevices()
  })
})
