var mongoose = require('mongoose')
var Schema = mongoose.Schema

const schema = new Schema({
  model: String,
  description: String
})

module.exports = mongoose.model('Device', schema)
