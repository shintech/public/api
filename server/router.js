const express = require('express')
const router = express.Router()
const User = require('./models/User')
const Device = require('./models/Device')

module.exports = function () {
  router.route('/users')
    .get((req, res) => {
      User.find()
        .exec((err, data) => {
          if (err) { res.send(err) }
          res.json(data)
        })
    })

  router.route('/users/:id')
    .get((req, res) => {
      User.findById(req.params.id)
        .exec((err, data) => {
          if (err) { res.send(err) }
          res.json(data)
        })
    })

  router.route('/devices')
    .get((req, res) => {
      Device.find()
        .exec((err, data) => {
          if (err) { res.send(err) }
          res.json(data)
        })
    })

  router.route('/devices/:id')
    .get((req, res) => {
      Device.findById(req.params.id)
        .exec((err, data) => {
          if (err) { res.send(err) }
          res.json(data)
        })
    })

  router.route('/')
    .get((req, res) => {
      res.status(200)
        .json({
          message: 'success'
        })
    })

  return router
}
