const express = require('express')
const bodyParser = require('body-parser')
const compression = require('compression')
const helmet = require('helmet')
const URL = require('url')

module.exports = ({ router }) => {
  const server = express()
    .use('/api', (req, res, next) => {
      const start = Date.now()
      const logger = req.app.get('logger')

      res.on('finish', () => {
        const finish = Date.now() - start

        const url = URL.format({
          host: `${req.get('host')}:${process.env['PORT']}`,
          pathname: req.originalUrl,
          port: process.env['PORT']
        })

        logger.info(`${res.statusCode} - ${req.method} - ${url} - ${finish}ms`)
      })

      next()
    })

    .use(compression())
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use(helmet())

  return server
}
