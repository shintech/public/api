module.exports = {
  Query: {
    devices: async (_, props, { dataSources }) => dataSources.devices.getAll(),
    device: async (_, props, { dataSources }) => dataSources.devices.getOne(props.id)
  }
}
