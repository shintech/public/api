module.exports = {
  Query: {
    users: async (_, props, { dataSources }) => dataSources.users.getAll(),
    user: async (_, props, { dataSources }) => dataSources.users.getOne(props.id)
  }
}
