const { DataSource } = require('apollo-datasource')
const Device = require('../../models/Device')

module.exports = class DevicesAPI extends DataSource {
  async getAll () {
    try {
      const devices = await Device.find()

      return devices
    } catch (err) {
      return new Error(err)
    }
  }

  async getOne (id) {
    try {
      const device = await Device.findById(id)

      return device
    } catch (err) {
      return new Error(err)
    }
  }
}
