const UsersAPI = require('./users')
const DevicesAPI = require('./devices')

module.exports = (REST_API) => () => ({
  users: new UsersAPI(REST_API),
  devices: new DevicesAPI(REST_API)
})
