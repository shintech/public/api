const { DataSource } = require('apollo-datasource')
const User = require('../../models/User')

module.exports = class UsersAPI extends DataSource {
  async getAll () {
    try {
      const users = await User.find()

      return users
    } catch (err) {
      return new Error(err)
    }
  }

  async getOne (id) {
    try {
      const user = await User.findById(id)

      return user
    } catch (err) {
      return new Error(err)
    }
  }
}
